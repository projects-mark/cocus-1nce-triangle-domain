package pt.cocus.nce.triangle.domain.entity;
/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 */
public class TriangleClassificationEntity extends AbstractAuditEntity {

    private static final long serialVersionUID = -7305020577256657433L;

    private long triangleClassificationId;
    private String triangleType;
    private long userId;
    private String triangleReferenceNumber;
    
    public long getTriangleClassificationId() {
        return triangleClassificationId;
    }
    public void setTriangleClassificationId(long triangleClassificationId) {
        this.triangleClassificationId = triangleClassificationId;
    }
    public String getTriangleType() {
        return triangleType;
    }
    public void setTriangleType(String triangleType) {
        this.triangleType = triangleType;
    }
    public long getUserId() {
        return userId;
    }
    public void setUserId(long userId) {
        this.userId = userId;
    }
    public String getTriangleReferenceNumber() {
        return triangleReferenceNumber;
    }
    public void setTriangleReferenceNumber(String triangleReferenceNumber) {
        this.triangleReferenceNumber = triangleReferenceNumber;
    }
        
}
