package pt.cocus.nce.triangle.domain.dao.query;

import java.time.LocalDate;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 */
public class TriangleClassificationQuery {

    private long triangleClassificationId;
    private String triangleType;
    private LocalDate triangleDateFrom;
    private LocalDate triangleDateTo;
    private String username;
    private String triangleReferenceNumber;
    
    public long getTriangleClassificationId() {
        return triangleClassificationId;
    }
    public void setTriangleClassificationId(long triangleClassificationId) {
        this.triangleClassificationId = triangleClassificationId;
    }
    public String getTriangleType() {
        return triangleType;
    }
    public void setTriangleType(String triangleType) {
        this.triangleType = triangleType;
    }
    public LocalDate getTriangleDateFrom() {
        return triangleDateFrom;
    }
    public void setTriangleDateFrom(LocalDate triangleDateFrom) {
        this.triangleDateFrom = triangleDateFrom;
    }
    public LocalDate getTriangleDateTo() {
        return triangleDateTo;
    }
    public void setTriangleDateTo(LocalDate triangleDateTo) {
        this.triangleDateTo = triangleDateTo;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getTriangleReferenceNumber() {
        return triangleReferenceNumber;
    }
    public void setTriangleReferenceNumber(String triangleReferenceNumber) {
        this.triangleReferenceNumber = triangleReferenceNumber;
    }
}
