package pt.cocus.nce.triangle.domain.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
public abstract class AbstractAuditEntity implements Serializable {

    private static final long serialVersionUID = -996212257501107329L;

    private Long createdBy;

    private LocalDateTime createdAt = LocalDateTime.now();

    private Long updatedBy;

    private LocalDateTime updatedAt;

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
    
}
