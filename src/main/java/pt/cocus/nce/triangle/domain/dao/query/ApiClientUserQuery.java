package pt.cocus.nce.triangle.domain.dao.query;
/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
public class ApiClientUserQuery {
    private long apiClientUserId;
    private String apiClientUserLogin;
    private String password;
    
    public long getApiClientUserId() {
        return apiClientUserId;
    }
    public void setApiClientUserId(long apiClientUserId) {
        this.apiClientUserId = apiClientUserId;
    }
    public String getApiClientUserLogin() {
        return apiClientUserLogin;
    }
    public void setApiClientUserLogin(String apiClientUserLogin) {
        this.apiClientUserLogin = apiClientUserLogin;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
   
}
