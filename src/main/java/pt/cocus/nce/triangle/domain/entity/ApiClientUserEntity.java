package pt.cocus.nce.triangle.domain.entity;
/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
public class ApiClientUserEntity extends AbstractAuditEntity {

    private static final long serialVersionUID = 271116068671407433L;

    private long userId;
    private String login;
    private String password;
    
    public long getUserId() {
        return userId;
    }
    public void setUserId(long userId) {
        this.userId = userId;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
