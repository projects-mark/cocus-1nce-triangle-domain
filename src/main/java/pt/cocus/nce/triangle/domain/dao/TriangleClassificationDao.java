package pt.cocus.nce.triangle.domain.dao;

import java.util.List;
import java.util.Optional;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import pt.cocus.nce.triangle.domain.dao.query.TriangleClassificationQuery;
import pt.cocus.nce.triangle.domain.entity.TriangleClassificationEntity;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 */

@Mapper
@Repository(value = TriangleClassificationDao.BEAN_NAME)
public interface TriangleClassificationDao {

    public static final String BEAN_NAME = "dao.TriangleClassificationDao";
    
    long insertTriangleClassification(final TriangleClassificationEntity entity);
    List<TriangleClassificationEntity> queryTriangleClassificationHistories();
    List<TriangleClassificationEntity> queryTriangleClassificationHistoriesViaLogin(final TriangleClassificationQuery query);
    Optional<TriangleClassificationEntity> queryTriangleClassificationHistory(final TriangleClassificationQuery query);
}
