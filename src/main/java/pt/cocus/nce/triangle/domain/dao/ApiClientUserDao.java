package pt.cocus.nce.triangle.domain.dao;

import java.util.Optional;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import pt.cocus.nce.triangle.domain.dao.query.ApiClientUserQuery;
import pt.cocus.nce.triangle.domain.entity.ApiClientUserEntity;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
@Mapper
@Repository(value =  ApiClientUserDao.BEAN_NAME)
public interface ApiClientUserDao {
    
    public static final String BEAN_NAME = "dao.ApiClientUserDao";
    
    Optional<ApiClientUserEntity> queryClientByUsername(final ApiClientUserQuery query);
    void insertApiClientUser(final ApiClientUserEntity entity);
    long updateApiClientUser(final ApiClientUserEntity entity);
    boolean isClientCredentialsValid(final ApiClientUserQuery query);
    

}
