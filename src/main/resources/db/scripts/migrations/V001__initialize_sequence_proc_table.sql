CREATE TABLE IF NOT EXISTS sequence (
  seq_name varchar(256) NOT NULL,
  current_val bigint NOT NULL,
  increment_val bigint NOT NULL DEFAULT '1',
  PRIMARY KEY (seq_name)
);

DELIMITER $$
create function currval(v_seq_name varchar(256)) RETURNS bigint
READS SQL DATA
DETERMINISTIC
begin     
    declare value integer;       
    set value = 0;       
    select current_val into value from sequence where seq_name = v_seq_name; 
   return value; 
end$$
DELIMITER ;

DELIMITER $$
create function nextval(v_seq_name varchar(256)) RETURNS bigint
READS SQL DATA
DETERMINISTIC
begin
    update sequence set current_val = current_val + increment_val where seq_name = v_seq_name;
    return currval(v_seq_name);
end$$
DELIMITER ;

