CREATE TABLE IF NOT EXISTS api_client_user (
    user_id bigint not null,
    login VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL, 
    created_at datetime NOT NULL,
    created_by bigint NOT NULL,
	updated_at datetime,
	updated_by bigint,
	PRIMARY KEY (user_id)
);

CREATE TABLE IF NOT EXISTS triangle_classification
(
  triangle_classification_id bigint NOT NULL,
  triangle_type VARCHAR(256) NOT NULL,
  created_at datetime NOT NULL,
  created_by bigint NOT NULL,
  updated_at datetime,
  updated_by bigint,
  user_id bigint NOT NULL,
  PRIMARY KEY (triangle_classification_id),
  constraint fk_ref_user_id_api_client_user FOREIGN KEY (user_id) REFERENCES api_client_user(user_id)
);
